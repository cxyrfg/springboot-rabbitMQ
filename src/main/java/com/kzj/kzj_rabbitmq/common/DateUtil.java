/*
 * Copyright 2013 NINGPAI, Inc.All rights reserved.
 * NINGPAI PROPRIETARY / CONFIDENTIAL.USE is subject to licence terms.
 */

package com.kzj.kzj_rabbitmq.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * 获取时间工具类
 */
public class DateUtil {

    /**
     * Date转String
     * @param
     * @return
     */
    public static String datetoString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }

    /**
     * String转Date
     * @return
     */
    public static Date stringTodate(String string) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.parse(string);
    }

    /**
     * 判断时间是否在时间段内
     * @param nowTime
     * @param beginTime
     * @param endTime
     * @return
     */
    public static boolean belongCalendar(Date nowTime, Date beginTime, Date endTime) {

        Calendar date = Calendar.getInstance();
        date.setTime(nowTime);

        Calendar begin = Calendar.getInstance();
        begin.setTime(beginTime);

        Calendar end = Calendar.getInstance();
        end.setTime(endTime);

        if (date.after(begin) && date.before(end)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 判断时间大小
     * @param date1
     * @param date2
     * @return 1大于 2小于 0等于
     */
    public static int compare_date(Date date1, Date date2) {
        if (date1.getTime() > date2.getTime()) {
            return 1;
        } else if (date1.getTime() < date2.getTime()) {
            return 2;
        } else {
            return 0;
        }
    }

    /**
     * 输入时间添加减少多少个小时
     * @param date
     * @param hours
     * @return
     */
    public static Date modifyHours(Date date, int hours) {
        Calendar dar = Calendar.getInstance();
        dar.setTime(date);
        dar.add(java.util.Calendar.HOUR_OF_DAY, hours);
        return dar.getTime();
    }

    public static int calLastedTime(Date startDate, Date endDate) {
        long a = endDate.getTime();
        long b = startDate.getTime();
        int c = (int)((a - b) / 1000);
        return c;
    }

    /**
     * 计算时间1 时间2距离现在时间最小  并计算返回跟当前时间的秒数
     * @param end_time 时间1
     * @param group_end_time  时间2
     * @param nowTime 当前时间
     * @return
     */
    public static int getMinimumTime(Date end_time, Date group_end_time, Date nowTime){
        if(end_time!=null && group_end_time!=null && nowTime!=null) {
            Date unite_end_time;
            if (compare_date(end_time, group_end_time) == 1) {
                unite_end_time = group_end_time;
            } else {
                unite_end_time = end_time;
            }
            return calLastedTime(nowTime, unite_end_time);
        }
        return 0;
    }

    public static void main(String[] args) {
        try {
            Date nowTime=stringTodate("2018-06-20 15:20:00");
            Date end_time= stringTodate("2018-06-20 15:30:00");
            Date group_end_time= stringTodate("2018-06-20 15:25:00");
            getMinimumTime(end_time,group_end_time,nowTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
